import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';

export interface Bill{
  name: string;
  value: number;
  totalReceived: number;
  receivedAt: number;
  admin: string;
  uid: string;
  emailIn: string;
}

@Injectable({
  providedIn: 'root'
})
export class CreateBillService {
  userId: String;

  private billCollection: AngularFirestoreCollection<Bill>;
  private Bills: Observable<Bill[]>;
  private afAuth: AngularFireAuth;
  constructor(db: AngularFirestore ) {
    this.billCollection = db.collection<Bill>('Bills');
    this.Bills = this.billCollection.snapshotChanges().pipe(
      map(actions =>{
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...data};
        });
      })
    );
  }

  getBills(){
    return this.Bills;
  }

  getBill(id){
    return this.billCollection.doc<Bill>(id).valueChanges();
  }

  updateBill(bill: Bill, id: string){
    return this.billCollection.doc(id).update(bill);
  }

  addBill(bill:Bill){
    return this.billCollection.add(bill);
  }

  removeBill(id){
    return this.billCollection.doc(id).delete();
  }
}
