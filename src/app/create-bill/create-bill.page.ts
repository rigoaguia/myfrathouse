import { Component, OnInit } from '@angular/core';
import { Bill, CreateBillService } from '../create-bill.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Component({
  selector: 'app-create-bill',
  templateUrl: './create-bill.page.html',
  styleUrls: ['./create-bill.page.scss'],
})
export class CreateBillPage implements OnInit {
  bill: Bill = {
    name: "",
    value: 0,
    totalReceived: 0,
    receivedAt: 0,
    admin: '',
    uid: '',
    emailIn: ''
  };

  billId = null;
  userId = null;
  constructor(private createBill: CreateBillService, private route: ActivatedRoute, private loadingController: LoadingController,
  private nav: NavController, private router: Router,public emailComposer: EmailComposer) { }

  ngOnInit() {
    this.billId = this.route.snapshot.params['id'];
    this.route.paramMap.subscribe(params => {
      this.userId = params.get("uid");
    });
    if(this.billId){
      this.loadBill();
    }


  }
  async loadBill(){
    const loading = await this.loadingController.create({

    });
    await loading.present();

    this.createBill.getBill(this.billId).subscribe(res => {
      loading.dismiss();
      this.bill = res;
    });
  }

  async saveBill(){
    const loading = await this.loadingController.create({

    });
    await loading.present();
    if(this.billId){
      this.createBill.updateBill(this.bill, this.billId).then(res =>{
        loading.dismiss();
        this.nav.pop();
      });
    }else{
      this.bill.uid = this.userId;
      this.createBill.addBill(this.bill).then(res =>{
        loading.dismiss();
        this.nav.pop();
      });
    }
  }

  senEmail(){

      let email = {
        to: this.bill.emailIn,
        cc: [],
        bcc: [],
        attachments: [],
        subject: 'My Frat House: conta',
        body: "Tipo de conta:"+this.bill.name+"\n"+"Valor da conta: R$"+this.bill.value+"\n"+
        "Total Recebido: R$"+this.bill.totalReceived+"\n"+"Data de recebimento da tarifa: "+
        this.bill.receivedAt+"\n"+"Administrador: "+this.bill.admin,
        isHtml: false,
        app: "Gamil"
      }

      // Send a text message using default options
      this.emailComposer.open(email);

    }



}
