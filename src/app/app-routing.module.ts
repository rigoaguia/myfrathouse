import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';



const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'bills', loadChildren: './bills/bills.module#BillsPageModule' },
  { path: 'bills/:id', loadChildren: './bills/bills.module#BillsPageModule' },
  { path: 'bill', loadChildren: './bill/bill.module#BillPageModule' },
  { path: 'create-bill', loadChildren: './create-bill/create-bill.module#CreateBillPageModule' },
  { path: 'create-bill/:uid/:id', loadChildren: './create-bill/create-bill.module#CreateBillPageModule' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule', canActivate:[AuthGuard] }, // tirar o canActive da pag. home e colocar em qual pagina deseja acessar apos o login
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule', canActivate:[LoginGuard] },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
