import { TestBed } from '@angular/core/testing';

import { CreateBillService } from './create-bill.service';

describe('CreateBillService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreateBillService = TestBed.get(CreateBillService);
    expect(service).toBeTruthy();
  });
});
