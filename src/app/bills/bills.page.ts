import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CreateBillService, Bill } from '../create-bill.service';
import { User } from '../interfaces/user';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-bills',
  templateUrl: './bills.page.html',
  styleUrls: ['./bills.page.scss'],
})
export class BillsPage implements OnInit {

  bills = [];
  bill: Bill[];
  id: string;

  sliderConfig = {
    spaceBetween: 10,
    centeredSlides: true,
    slidesPerView: 1.5

  }

  constructor(private router: Router, private route: ActivatedRoute, private createBillService: CreateBillService ) { }
  userID = null;
  ngOnInit() {
    this.userID = this.route.snapshot.params['id'];
    console.log(this.userID);
    if(this.userID){
    this.createBillService.getBills().subscribe(res =>{
      this.bill = res;
    });
  }
  console.log(this.bill);

  }

  goTo(item){
    console.log(item[0]);
    this.router.navigate(["create-bill", this.userID, item[0]]);
  }

  openBill(){
    this.router.navigate(["bill"]);
  }

  openCreateBill(){
    this.router.navigate(["create-bill", this.userID, ""])
  }

  removeBill(id){
    this.createBillService.removeBill(id);
  }



}
