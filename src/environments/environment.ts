// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {

    apiKey: "AIzaSyAnLQ2R_5vFrPwBTUupmC8vset8s8sNJOM",
    authDomain: "myfrathouse-cb551.firebaseapp.com",
    databaseURL: "https://myfrathouse-cb551.firebaseio.com",
    projectId: "myfrathouse-cb551",
    storageBucket: "",
    messagingSenderId: "1090754853664",
    appId: "1:1090754853664:web:2f0a54150ce29d69"

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
